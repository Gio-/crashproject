﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public PlayerEntity player;

    private Vector3 _dir;

    private void Awake()
    {
        player = GameManager.instance.Player;
    }

    private void Update()
    {
        // Jumping
        if (player.isGrounded || !player.isGrounded && player.jumpCount < player.maxJumpCount)
        {
            if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown("joystick button 0"))
            {
                player.motionController.Jump(player.jumpForce);
                player.isGrounded = false;
                player.anim.SetBool("isGrounded", false);
                player.jumpCount += 1;

                if (player.jumpCount >= player.maxJumpCount)
                    player.anim.SetTrigger("isDoubleJumping");
            }
        }

        // Attack
        if (!player.isAttacking)
        {
            if(Input.GetMouseButtonDown(0) || Input.GetKeyDown("joystick button 2"))
            {
                player.anim.SetTrigger("isAttacking");
                player.Attack();
            }
        }

        // Apply Direction
        _dir = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        _dir = Camera.main.transform.TransformDirection(_dir);
        _dir.y = 0.0f;

        player.anim.SetFloat("Forward", new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")).magnitude);
    }

    void FixedUpdate()
    {
        player.motionController.Move(_dir);
    }
}

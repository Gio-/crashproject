﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UIManager : MonoBehaviour {


    public UILabel wumpaFruitLabel;
    public UILabel lifeLabel;
    public UISprite[] tokensContainer = new UISprite[3];

    public TweenAlpha[] hudAnim = new TweenAlpha[3];
    private bool callHudAnim;

    [HideInInspector]
    public int wumpafruits;
    [HideInInspector]
    public int lifes;
    [HideInInspector]
    public int tokens;

    public static UIManager instance = null;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(this);
        }
        for (int i = tokensContainer.Length - 1; i >= 0; i--)
        {
            tokensContainer[i].gameObject.SetActive(false);
        }
    }

    public void ShowHUD()
    {
        for (int i = hudAnim.Length - 1; i >= 0; i--)
        {
            hudAnim[i].PlayForward();
        }
        StartCoroutine("CloseHUD");

    }

    public void HideHUD()
    {
        for (int i = hudAnim.Length - 1; i >= 0; i--)
        {
            hudAnim[i].PlayReverse();
        }
    }

    public IEnumerator CloseHUD()
    {
        yield return new WaitForSeconds(5.0f);
        HideHUD();
        yield return null;

    }

    public void AddToken()
    {
        for (int i = 0; i < tokens; i++)
        {
            if (tokensContainer[i].gameObject.activeSelf != true)
                tokensContainer[i].gameObject.SetActive(true);
        }
    }

}

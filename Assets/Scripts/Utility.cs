﻿
using UnityEngine;

namespace Utility
{
    public static class MyComponents
    {
        public static T MyGetComponent<T>(this GameObject g) where T : Component
        {
            T foundComponent = g.GetComponent<T>();
            if (foundComponent == null)
                foundComponent = g.AddComponent<T>();

            return foundComponent;
        }
    }
}

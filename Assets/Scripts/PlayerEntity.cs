﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntity : BaseEntity {

    public enum HitDirection
    {
        UP,
        DOWN,
        NULL
    }

    public HitDirection hitDirection;

    [Header("General Variable")]
    public float jumpForce;
    public bool isGrounded;
    public bool isAttacking;
    public int maxJumpCount;
    public float attackRadius;
    public float attackDelay;

    [HideInInspector] public int jumpCount;

    [Header("Components Variable")]
    public MotionController motionController;
    public Animator anim;

    private RaycastHit hit;
    private Ray ray;
    private Vector3 forwardVelocity;

    public override void OnAwake()
    {
        motionController = GetComponent<MotionController>();
        anim = GetComponent<Animator>();

        forwardVelocity = new Vector3(motionController.rb.velocity.x, 0.0f, motionController.rb.velocity.z);
    }

    public void Attack()
    {
        isAttacking = true;
        StartCoroutine(DoAttack());
    }

    IEnumerator DoAttack()
    {
        Collider[] hitted = Physics.OverlapSphere(transform.position, attackRadius);
        
        foreach(Collider objHitted in hitted)
        {
            if (ObjectPooling.instance.entities.ContainsKey(objHitted.gameObject))
            {
                ObjectPooling.instance.entities[objHitted.gameObject].OnDeath();
            }
        }
        
        yield return new WaitForSeconds(attackDelay); 
        isAttacking = false;
    }

    public void FixedUpdate()
    {
        if(motionController.rb.velocity.y < -0.1f)
            anim.SetBool("isFalling", true);

        anim.SetBool("isGrounded", isGrounded);
    }

    public void OnCollisionEnter(Collision other)
    {
        anim.SetBool("isGrounded", true);
        anim.SetBool("isFalling", false);

        if (!isGrounded)
        {
            // Ottengo la direzione di dove ha colpito.
            hitDirection = GetHitDirection(other);

            if (ObjectPooling.instance.entities.ContainsKey(other.collider.gameObject))
            {
                if (!isAttacking)
                {
                    if (hitDirection == HitDirection.UP)
                    {
                        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("JumpingBox"))
                        {
                            ObjectPooling.instance.entities[other.collider.gameObject].OnJumpOn();
                            // Jump Up

                        }
                    }
                    else if (hitDirection == HitDirection.DOWN)
                    {
                        if (other.gameObject.CompareTag("Enemy"))
                        {
                            this.OnDeath();
                        }
                        if (other.gameObject.CompareTag("JumpingBox"))
                        {
                            ObjectPooling.instance.entities[other.collider.gameObject].OnJumpOn();
                            // Jump Down
                        }
                    }
                }
                else
                {
                    ObjectPooling.instance.entities[other.collider.gameObject].OnDeath();
                }
            }
        }

        isGrounded = true;
        jumpCount = 0;
    }

    public void OnCollisionStay(Collision collision)
    {
        isGrounded = true;
    }

    public void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
        hitDirection = HitDirection.NULL;
    }

    protected HitDirection GetHitDirection(Collision other)
    {
        Vector3 direction = (this.transform.position - other.transform.position).normalized;
        ray = new Ray(this.transform.position, direction);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider != null)
            {
                Vector3 localPoint = hit.transform.InverseTransformPoint(hit.point);
                Vector3 localDir = localPoint.normalized;

                float upDot = Vector3.Dot(localDir, Vector3.up);

                if (upDot > 0)
                {
                    return HitDirection.UP;
                }
                else 
                {
                    return HitDirection.DOWN;
                }

            }
        }

        return HitDirection.NULL;
    }
}

﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour {

    public static ObjectPooling instance = null;

    public GameObject life;
    public GameObject score;
    public GameObject girl;

    public Dictionary<GameObject, BaseEntity> entities = new Dictionary<GameObject, BaseEntity>();
    public Dictionary<string, List<PickupController>> pickup = new Dictionary<string, List<PickupController>>();

    private void Awake()
    {
        // Singleton
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        // Trovo tutte le entità nella scena.
        BaseEntity[] entitiesFounded = FindObjectsOfType<BaseEntity>();

        // Assegno le entità trovate al dictionary.
        for (int i = 0; i < entitiesFounded.Length; i++)
        {
            entities.Add(entitiesFounded[i].gameObject, entitiesFounded[i]);
        }

        pickup.Add("Apple", new List<PickupController>());
        for (int i = 0; i < 40; i++)
        {
            
            GameObject pkp = Instantiate(score, this.transform);

            
            pickup["Apple"].Add(pkp.GetComponent<PickupController>());

            pkp.SetActive(false);
        }

        pickup.Add("Life", new List<PickupController>());
        pickup.Add("Girl", new List<PickupController>());
        for (int i = 0; i < 5; i++)
        {
            GameObject pkp = Instantiate(life, this.transform);

            
            pickup["Life"].Add(pkp.GetComponent<PickupController>());

            pkp.SetActive(false);

            pkp = Instantiate(girl, this.transform);

           
            pickup["Girl"].Add(pkp.GetComponent<PickupController>());

            pkp.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraFollower;
    public float speed = 0.1f;
    public float rotation = .1f;
    RaycastHit hit;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, cameraFollower.position, speed * Time.deltaTime);
             
        if(Physics.Raycast(transform.position, Vector3.down, out hit, 10f))
        {
            if (!hit.collider.CompareTag("Player"))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, hit.transform.rotation, .1f);
            }
        }
       
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEntity : MonoBehaviour, IEntity {

    private void Awake()
    {
        OnAwake();
    }

    public virtual void OnAwake()
    {

    }

    public virtual void OnDeath()
    {

    }

    public virtual void OnJumpOn()
    {

    }

    public virtual void OnGetDamage()
    {

    }
}
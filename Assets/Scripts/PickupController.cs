﻿using System.Collections;
using UnityEngine;

public class PickupController : MonoBehaviour {
    
    public enum PickupType
    {
        LIFE,
        SCORE,
        GIRL
    }

    public PickupType pickupType;

    private IEnumerator updateAsync;

    public void OnPickup()
    {
        switch (pickupType)
        {
            case PickupType.SCORE:
                UIManager.instance.wumpafruits += 1;
                UIManager.instance.wumpaFruitLabel.text = UIManager.instance.wumpafruits.ToString();
                ObjectPooling.instance.pickup["Apple"].Add(this);
                
                break;
            case PickupType.GIRL:
                UIManager.instance.tokens += 1;
                UIManager.instance.AddToken();
                ObjectPooling.instance.pickup["Girl"].Add(this);
                break;
            case PickupType.LIFE:
                UIManager.instance.lifes += 1;
                UIManager.instance.lifeLabel.text = UIManager.instance.lifes.ToString();
                ObjectPooling.instance.pickup["Life"].Add(this);
                break;
        }
        this.gameObject.SetActive(false);
        if (UIManager.instance.wumpafruits >= 100)
        {
            UIManager.instance.wumpafruits -= 100;
            UIManager.instance.lifes += 1;
            UIManager.instance.wumpaFruitLabel.text = UIManager.instance.wumpafruits.ToString();
            UIManager.instance.lifeLabel.text = UIManager.instance.lifes.ToString();

        }
        UIManager.instance.ShowHUD();

    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            OnPickup();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SkunkEntity : BaseEntity
{

    public float respawnTime = 3.0f;
    public Transform startPos;
    public Transform endPos;
    public MotionController motionController;
    private bool respawnRequested;
    private MeshRenderer mr;

    bool hasPathEnded
    {
        get
        {
            return (motionController.agent.remainingDistance != Mathf.Infinity
             && motionController.agent.pathStatus == NavMeshPathStatus.PathComplete
             && motionController.agent.remainingDistance == 0);
        }
    }

    public override void OnAwake()
    {
        motionController = GetComponent<MotionController>();
        mr = GetComponentInChildren<MeshRenderer>();
    }

    public void Start()
    {
        motionController.Move(endPos.position);
        StartCoroutine(CheckForPath());
    }

    IEnumerator CheckForPath()
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);

            if (hasPathEnded)
            {
                mr.enabled = false;
                this.gameObject.transform.position = startPos.position;
                motionController.Stop();

                yield return new WaitForSeconds(respawnTime);

                mr.enabled = true;
                motionController.agent.isStopped = false;

            }

            yield return null;
        }

    }

    public override void OnJumpOn()
    {
        StopCoroutine(CheckForPath());

        transform.rotation = Quaternion.Euler(0, 0, 180.0f);
        this.gameObject.SetActive(false);
    }

}


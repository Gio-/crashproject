﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public bool persistent { get; set; }
    private bool destroyRequested = false;

    private static GameManager _instance = null;
    public static GameManager instance
    {
        get
        {
            if (_instance == null || !_instance.destroyRequested)
            {
                GameManager[] objects = FindObjectsOfType<GameManager>();
                if (objects.Length > 0)
                    _instance = objects[0];
                if (objects.Length > 1)
                {
                    for (int i = 1; i < objects.Length; i++) Destroy(objects[i].gameObject);
                }
                else if (_instance == null)
                {
                    GameObject gameObject = new GameObject();
                    gameObject.name = ("GameManager");
                    _instance = gameObject.AddComponent<GameManager>();
                }

                DontDestroyOnLoad(_instance.gameObject);
                _instance.persistent = true;

                _instance.gameObject.transform.position = Vector3.zero;
                _instance.gameObject.transform.rotation = Quaternion.identity;
                _instance.gameObject.transform.localScale = Vector3.one;
            }
            return _instance;
        }
    }


    private PlayerEntity m_player;
    public PlayerEntity Player
    {
        get
        {
            return m_player ?? (m_player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerEntity>());
        }
    }

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;


    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (!persistent) Destroy(gameObject);
    }

    private void OnDestroy()
    {
        destroyRequested = true;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


}
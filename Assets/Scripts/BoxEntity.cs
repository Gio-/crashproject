﻿using System.Collections.Generic;
using UnityEngine;

public class BoxEntity : BaseEntity
{
    public BoxBehaviour boxBehaviour;
    private BoxCollider bc;

    public override void OnAwake()
    {
        bc = this.GetComponent<BoxCollider>();
        boxBehaviour = ScriptableObject.Instantiate(boxBehaviour);

        boxBehaviour.DoWhenQuantityEnded = OnEndQuantity;
    }

    public override void OnDeath()
    {
        boxBehaviour.OnDeath(this.transform.position);

        //bc.enabled = false;
        this.gameObject.SetActive(false);
        // Animazione di distruzione
    }

    public override void OnJumpOn()
    {
        boxBehaviour.OnJumpOn(this.transform.position);

        // Animazione di salto
    }

    public override void OnGetDamage()
    {
        boxBehaviour.OnGetDamage();

        // Animazione di danno
    }

    public void OnEndQuantity()
    {
        this.gameObject.SetActive(false);
    }
}
﻿// Interfaccia che definisce i metodi fondamentali di 
// qualsiasi entità nel gioco. Boxes, Enemies, Player.

public interface IEntity {

    // Quando l'entità viene distrutta.
    void OnDeath();

    // Quando qualcuno salta sull'entità 
    // la direzione non è importante.
    void OnJumpOn();

    // Quando l'entità subisce danno.
    void OnGetDamage();

}
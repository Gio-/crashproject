﻿using UnityEngine;
using UnityEngine.AI;

public abstract class IMotion
{
    public abstract void DoMotion(Vector3 pos);
    public abstract void DoStop();
    public abstract void DoJump(float height);
}

public class Agent : IMotion
{
    private NavMeshAgent agent;
    public Agent(NavMeshAgent _agent) { agent = _agent; }
    
    public override void DoMotion(Vector3 pos)
    {
        agent.SetDestination(pos);
    }

    public override void DoStop()
    {
        agent.isStopped = true;
    }

    public override void DoJump(float height)
    {

    }
}

public class Force : IMotion
{
    private Rigidbody rb;
    public float force;
    public ForceMode forceMode;
    public Force(Rigidbody _rb, float _force = 100.0f, ForceMode _forceMode = ForceMode.Force) { rb = _rb; forceMode = _forceMode; force = _force; }

    public override void DoMotion(Vector3 pos)
    {
        rb.AddForce(pos * force * Time.fixedDeltaTime, forceMode);
    }

    public override void DoStop()
    {
        rb.velocity = new Vector3(0,0,0);
    }

    public override void DoJump(float height)
    {
        rb.AddForce(new Vector3(0, height, 0) * force * Time.fixedDeltaTime, forceMode);
    }
}

public class Velocity : IMotion
{
    private Vector3 _dir;
    private Rigidbody rb;
    public float force;
    public Velocity(Rigidbody _rb, float _force = 100.0f) { rb = _rb; force = _force; }
    
    public override void DoMotion(Vector3 pos)
    {
        rb.velocity = new Vector3(pos.x * force * Time.fixedDeltaTime, rb.velocity.y + pos.y + Physics.gravity.y * Time.fixedDeltaTime, pos.z * force * Time.fixedDeltaTime);
        _dir = new Vector3(rb.velocity.x, 0.0f, rb.velocity.z);

        if (_dir != Vector3.zero)
        {
            rb.gameObject.transform.rotation = Quaternion.LookRotation(_dir, Vector3.up);
        }
        
    }

    public override void DoStop()
    {
        rb.velocity = new Vector3(0, 0, 0);
    }

    public override void DoJump(float height)
    {
        rb.velocity += new Vector3(0, height, 0);
    }
}

public class MotionController : MonoBehaviour {

    public enum MotionType
    {
        NAVMESH,
        FORCE,
        VELOCITY
    }

    public MotionType motionType;

    public IMotion myMotion;

    // NavMesh
    public NavMeshAgent agent;

    // Rb
    public Rigidbody rb;
    public float force;
    public ForceMode forceMode;

    void Awake()
    {
        switch (motionType)
        {
            case MotionType.NAVMESH:
                agent = Utility.MyComponents.MyGetComponent<NavMeshAgent>(this.gameObject);
                myMotion = new Agent(agent);
                break;
            case MotionType.FORCE:
                rb = Utility.MyComponents.MyGetComponent<Rigidbody>(this.gameObject);
                myMotion = new Force(rb, force, forceMode);
                break;
            case MotionType.VELOCITY:
                rb = Utility.MyComponents.MyGetComponent<Rigidbody>(this.gameObject);
                myMotion = new Velocity(rb, force);
                break;
        }
    }

    public void Move(Vector3 direction)
    {
        myMotion.DoMotion(direction);
    }

    public void Stop()
    {
        myMotion.DoStop();
    }

    public void Jump(float height)
    {
        myMotion.DoJump(height);
    }

}
﻿using UnityEngine;

public class BoxBehaviour : ScriptableObject, IEntity {


    public System.Action DoWhenQuantityEnded;

    public virtual void OnDeath()
    {

    }

    public virtual void OnDeath(Vector3 spawnPosition)
    {

    }

    public virtual void OnJumpOn()
    {

    }

    public virtual void OnJumpOn(Vector3 spawnPosition)
    {

    }

    public virtual void OnGetDamage()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Box Creator/Box Random")]
public class BoxRandom : BoxBehaviour
{

    // Quantità di mele presenti al suo interno.
    [Header("Apple Quantities")]
    [HideInInspector] public int quantity;
    [Range(10, 20)]
    public int maxQuantity;
    [Range(1, 10)]
    public int minQuantity;

    public PickupController.PickupType objectToSpawn;

    private GameObject _objectSpawned;
    private string database;
    Vector3 randomPos;

    public void OnEnable()
    {
        database = GetName(objectToSpawn);
        quantity = Random.Range(minQuantity, maxQuantity);
    }

    public override void OnDeath(Vector3 spawnPosition)
    { 
        // Cadono tutte le mele restanti
        if (quantity > 0)
        {
            for (int i = 0; i < quantity; i++)
            {
                SpawnPickupable(spawnPosition);
            }
        }
    }

    public void SpawnPickupable(Vector3 spawnPosition)
    {
        if (ObjectPooling.instance.pickup[database].Count > 0)
        {
            randomPos = Random.insideUnitCircle * 1;
            _objectSpawned = ObjectPooling.instance.pickup[database][0].gameObject;
            _objectSpawned.transform.position = new Vector3(spawnPosition.x + randomPos.x, spawnPosition.y+1, spawnPosition.z + randomPos.y);
            _objectSpawned.SetActive(true);
            ObjectPooling.instance.pickup[database].Remove(ObjectPooling.instance.pickup[database][0]);

        }

        _objectSpawned = null;
    }

    string GetName(PickupController.PickupType _objectToSpawn)
    {
        string _result;
        switch (_objectToSpawn)
        {
            default:
                _result = "Apple";
                break;
            case PickupController.PickupType.GIRL:
                _result = "Girl";
                break;
            case PickupController.PickupType.LIFE:
                _result = "Life";
                break;
        }

        return _result;
    }
}
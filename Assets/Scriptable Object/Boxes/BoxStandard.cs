﻿using UnityEngine;

[CreateAssetMenu(menuName = "Box Creator/Box Standard")]
public class BoxStandard : BoxBehaviour {

    public float force = 10.0f;

    public bool giveJumpForce;
    public bool giveFruit;
    public bool giveOnDeath;
    public bool destroyWhenQuantityEnd;

    public int quantity;
    public PickupController.PickupType objectToSpawn;

    GameObject _objectSpawned;
    Vector3 randomPos;
    string database;

    public void OnEnable()
    {
        database = GetName(objectToSpawn);
    }

    public override void OnJumpOn(Vector3 spawnPosition)
    {
        if (!giveJumpForce) return;

        // Fa saltare il player
        GameManager.instance.Player.motionController.Jump(force);

        if (giveFruit)
        {
            // Spawna una mela ad ogni salto
            quantity = (quantity - 1 > 0) ? quantity - 1 : 0;

            if(quantity > 0)
            {
                SpawnPickupable(spawnPosition);
            }

            if (quantity == 0)
            {
                if (destroyWhenQuantityEnd)
                {
                    DoWhenQuantityEnded();
                }
                OnDeath();
            }
            
        }

        base.OnJumpOn(spawnPosition);
    }

    public override void OnDeath(Vector3 spawnPosition)
    {
        if (!giveFruit) return;
        if (giveOnDeath)
        // Cadono tutte le mele restanti
        {
            if (quantity > 0)
            {
                for (int i = 0; i < quantity; i++)
                {
                    SpawnPickupable(spawnPosition);
                }
            }
        }

        
        
    }

    public void SpawnPickupable(Vector3 spawnPosition)
    {
        if (ObjectPooling.instance.pickup[database].Count > 0)
        {
            randomPos = Random.insideUnitCircle * 1;
            _objectSpawned = ObjectPooling.instance.pickup[database][0].gameObject;
            _objectSpawned.transform.position = new Vector3(spawnPosition.x + randomPos.x, spawnPosition.y+1, spawnPosition.z + randomPos.y);
            _objectSpawned.SetActive(true);
            ObjectPooling.instance.pickup[database].Remove(ObjectPooling.instance.pickup[database][0]);

        }

        _objectSpawned = null;
    }

    string GetName(PickupController.PickupType _objectToSpawn)
    {
        string _result;
        switch (_objectToSpawn)
        {
            default:
                _result = "Apple";
                break;
            case PickupController.PickupType.GIRL:
                _result = "Girl";
                break;
            case PickupController.PickupType.LIFE:
                _result = "Life";
                break;
        }

        return _result;
    }
}
